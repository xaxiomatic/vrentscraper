package main

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"
)

func TestArrayAvg(t *testing.T) {
	var a = []float64{1.0, 2.0, 3.0}
	expected := 2.0
	if arrayAvg(&a) != expected {
		t.Errorf("Avg was incorrect, got: %f, want: %f", arrayAvg(&a), expected)
	}
}

func TestGetOffers(t *testing.T) {
	mockedResponse := `
		{  
		   "response":{  
			  "data":{  
				 "offers":{  
					"offers":[  
					   1,
					   2
					]
				 }
			  }
		   }
		}
	`
	var unMarshaledResponse interface{}
	responseBytes := []byte(mockedResponse)
	json.Unmarshal(responseBytes, &unMarshaledResponse)
	offers := getOffers(&unMarshaledResponse)
	if len(*offers) != 2 {
		t.Error("getOffers unable to extract offers element")
	}
}

func TestGetTime(t *testing.T) {
	now := time.Now()
	now = now.AddDate(0, 0, 7)
	layout := "2006-01-02T15:04:05"
	requestedTime, _ := time.Parse(layout, getTime(1))
	now_year, now_week := now.ISOWeek()
	requested_year, requested_week := requestedTime.ISOWeek()
	if now_year != requested_year || now_week != requested_week {
		t.Error("Weeks not equal")
	}
}

func testForKey(dict *tIfMap, t *testing.T, key string) {
	if _, ok := (*dict)[key]; !ok {
		t.Error(key + " not in body")

	}
}

func TestPostContent(t *testing.T) {
	postBody := postContent()
	var unmarshaledBody interface{}
	json.Unmarshal([]byte(postBody), &unmarshaledBody)
	dict := unmarshaledBody.(tIfMap)
	testForKey(&dict, t, "pickupLocationID")
	testForKey(&dict, t, "dropoffLocationID")
	testForKey(&dict, t, "pickupAt")
	testForKey(&dict, t, "dropoffAt")
	testForKey(&dict, t, "residenceCountry")
	testForKey(&dict, t, "driverAge")
}

func TestGetQuoteInfo(t *testing.T) {
	mockedQuoteMap := `
		{  
		   "package":{  
			  "name":"fakeName"
		   },
		   "price":{  
			  "offer":{  
				 "total":{  
					"converted":{  
					   "amount":1.7
					}
				 }
			  }
		   }
		}
	`
	var unMarshaledQuote interface{}
	responseBytes := []byte(mockedQuoteMap)
	json.Unmarshal(responseBytes, &unMarshaledQuote)
	quote := unMarshaledQuote.(tIfMap)
	amount, pkg := getQuoteInfo(quote)

	if amount != 1.7 {
		t.Error("Invalid amount")
	}

	if pkg != "fakeName" {
		t.Error("Invalid package name")
	}
}

func TestAggregatePriceInfo(t *testing.T) {
	offers := providerOffers{
		[]float64{100.0, 200.0, 300.0},
		[]float64{400.0, 500.0, 600.0},
	}
	count, avgRelax, avgInclusive, avgCombined := aggregatePriceInfo(&offers)
	if count != "3" {
		t.Error("Invalid offer count")
	}
	if avgRelax != "5.00 €" {
		t.Error("Invalid average for Relax package")
		fmt.Print(avgRelax)
	}
	if avgInclusive != "2.00 €" {
		t.Error("Invalid average for Inclusive package")
	}
	if avgCombined != "3.50 €" {
		t.Error("Invalid average for combined")
	}
}

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil { return true, nil }
	if os.IsNotExist(err) { return false, nil }
	return true, err
}

func TestEntryPoint(t *testing.T) {
	const fPath = "./price_summary.csv"
	if ok,_ := exists(fPath); ok{
		os.Remove("./price_summary.csv")
	}
	main()
	if ok,_ := exists(fPath); !ok{
		t.Error("Summary file not created")
	}
}
