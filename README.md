Simple script to fetch pricing information from vehicle https://api.vehicle-rent.com/get-offers endpoint and produce a
summary csv report. 

The report is dumped to price_summary.csv in the current directory.
The content is one provider per line with the following aggregated datapoints for the provider

- Count of offers for one provider
- Average price for "Relax" package
- Average price for "Inclusive" package
- Average price of both packages

For simplicity's sake we use a lot of hardcoded things and globals and having everything declared in the main package
so there are some good practices being broken.

Run this with either : ```go run main.go``` or compile and run with ```go build && ./rentScraper```

This will dump everything to stdout ```go run main.go && cat price_summary.csv```  