package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

// Simple script to fetch pricing information from vehicle https://api.vehicle-rent.com/get-offers endpoint and produce a
// summary csv report. The report is dumped to price_summary.csv in the current directory.
// The content is one provider per line with the following aggregated datapoints for the provider
// - Count of offers for one provider
// - Average price for "Relax" package
// - Average price for "Inclusive" package
// - Average price of both packages
// For simplicity's sake we use a lot of hardcoded things and globals so this is not meant to be close to production level code

// Holds all offer for one provider broken down by package
// A single position represents prices for one offer
type providerOffers struct {
	inclusive []float64
	relax     []float64
}

// Mapped offer by provider name
var mappedOffers = make(map[string]*providerOffers)

type tIfMap = map[string]interface{}
type tIf = []interface{}

const ExitNoFile = 3
const ExitWebServiceDataFormat = 2
const ExitWebService = 1

// Returns an ISO-8601 formatted date string where the timepoint represented is time.Now() + numWeeks
func getTime(numWeeks int) string {
	now := time.Now()
	desiredDate := now.AddDate(0, 0, 7*numWeeks)
	return desiredDate.Format(time.RFC3339)[:19] // We adjust for the upstream not supporting tzinfo
}

// Returns the post content we use to fetch the data from the get-offers endpoint.
// LocationIds are hardcoded for simplicity's sake and the only variable parameters are
// pickupAt and dropoffAt which are set to now + 1 week and now + 2 weeks respectively.
// We use the location id of "Zagreb Airport" for the query.
func postContent() string {
	return `
		{  
		   "pickupLocationID":"ae0cca53-b6b1-4bc9-a52b-4986386aac9d",
		   "dropoffLocationID":"ae0cca53-b6b1-4bc9-a52b-4986386aac9d",
		   "pickupAt":"` + getTime(1) + `",
		   "dropoffAt":"` + getTime(2) + `",
		   "residenceCountry":"SI",
		   "driverAge":30
		}`
}

// Preforms a POST to /get-offers reads the entire stream and returns.
// The body of the request is dictated by the postContent function and this defines our actual query.
func fetchGetOffers() []byte {
	const url = "https://api.vehicle-rent.com/get-offers"
	request, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(postContent())))
	if err != nil{
		log.Print(err)
		os.Exit(ExitWebService)
	}
	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept-Currency", "EUR") // instruct the server to do the conversion for us as the rates are fundamentaly broken for USD
	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil || response.StatusCode != 200 {
		log.Print(err)
		log.Print(fmt.Sprintf("Fetching data from : %s failed", url))
		os.Exit(ExitWebService)
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Print(err)
		log.Print("Reading from response stream failed")
		os.Exit(ExitWebService)
	}
	return body
}

// Extracts price and package name from the quote.
func getQuoteInfo(quote tIfMap) (float64, string) {
	price := quote["price"].(tIfMap)
	offer := price["offer"].(tIfMap)
	total := offer["total"].(tIfMap)
	converted := total["converted"].(tIfMap)
	amount := converted["amount"].(float64)
	pkg := quote["package"].(tIfMap)
	name := pkg["name"].(string)
	return amount, name
}

// Maps a single offer by providers name extracting prices for Relax and Inclusive packages.
func mapOffer(offer *tIfMap) {
	branch := (*offer)["pickupBranch"].(tIfMap)
	vendor := branch["vendor"].(tIfMap)
	vendorName := vendor["name"].(string)
	if _, hasVendor := mappedOffers[vendorName]; !hasVendor {
		mappedOffers[vendorName] = new(providerOffers)
	}
	quotes := (*offer)["quotes"].(tIf)
	for _, quote := range quotes {
		amount, pkg := getQuoteInfo(quote.(tIfMap))
		switch pkg {
		case "Relax":
			mappedOffers[vendorName].inclusive = append(mappedOffers[vendorName].inclusive, amount)
		case "Inclusive":
			mappedOffers[vendorName].relax = append(mappedOffers[vendorName].relax, amount)
		default:
			log.Print("Unknown package found: " + pkg)
		}
	}
}

// Runs a loop over the offers array of the response and individually maps each offer to mappedOffers.
func mapOffers(offers *tIf) {
	for _, offer := range *offers {
		o := offer.(tIfMap)
		mapOffer(&(o))
	}
}

// Helper function to get at the element that gets the final offers list from the response.
func getOffers(dataMap *interface{}) *tIf {
	mappedData := (*dataMap).(tIfMap)
	response := mappedData["response"].(tIfMap)
	data := response["data"].(tIfMap)
	offers := data["offers"].(tIfMap)
	offersList := offers["offers"].(tIf) // the actual structure is nested twice with "offer" key so not a bug :)
	return &offersList
}

// Helper function to calculate the arithmetic average of values passed in arr.
func arrayAvg(arr *[]float64) float64 {
	var total float64 = 0
	for _, value := range *arr {
		total += value
	}
	return total / float64(len(*arr))
}

// Returns the aggregate values for a single provider from data passed in offers.
// Returned in order are:
// - Count of offers for one provider
// - Average price for "Relax" package
// - Average price for "Inclusive" package
// - Average price of both packages
// For easier handling we already round the prices and format them for final output.
func aggregatePriceInfo(offers *providerOffers) (string, string, string, string) {
	relaxAvg := arrayAvg(&offers.relax) / 100
	inclusiveAvg := arrayAvg(&offers.inclusive) / 100
	// We are dealing with the same number of elements so an average of averages yields the correct result
	combinedAvg := (relaxAvg + inclusiveAvg) / 2
	return strconv.Itoa(len(offers.inclusive)),
		fmt.Sprintf("%.2f €", relaxAvg),
		fmt.Sprintf("%.2f €", inclusiveAvg),
		fmt.Sprintf("%.2f €", combinedAvg)
}

// Writes the summary of price information to a csv file called price_summary.
// One line holds aggregated prices for a single provider.
func outputSummary(data *map[string]*providerOffers) {
	file, err := os.Create("./price_summary.csv")
	if err != nil {
		log.Print(err)
		os.Exit(ExitNoFile)
	}
	defer file.Close()
	_, err = file.WriteString("Provider|Count offers|Average price Relax|Average price Inclusive|Average price combined\n")
	if err != nil{
		log.Print("Error while writing summery file")
		os.Exit(ExitNoFile)
	}
	for provider, offers := range *data {
		countOffers, priceRelax, priceInclusive, priceCombined := aggregatePriceInfo(offers)
		_, err := file.WriteString(provider + "|" + countOffers + "|" + priceRelax + "|" + priceInclusive + "|" + priceCombined + "\n")
		if err != nil{
			log.Print("Error while writing summery file")
			os.Exit(ExitNoFile)
		}
	}
}

// Main entrypoint function. In order:
// - Calls the web service endpoint
// - Transforms the returned response data to a map indexed by providers name
// - Outputs aggregated data about the offers to a csv file
func main() {
	var unMarshaledResponse interface{}
	responseBytes := fetchGetOffers()
	err := json.Unmarshal(responseBytes, &unMarshaledResponse)
	if err != nil {
		log.Print(err)
		log.Print("Failed unmarshaling response from service")
		os.Exit(ExitWebServiceDataFormat)
	}
	offers := getOffers(&unMarshaledResponse)
	mapOffers(offers)
	outputSummary(&mappedOffers)
}
